package mock

import "github.com/dgrijalva/jwt-go"

type MockJWT struct {
	BasicMock
}

func (mj MockJWT) Parse(tokenStr string) (*jwt.Token, error) {
	f := mj.GetMockFunc("Parse")
	execFunc := f.(func(tokenStr string) (*jwt.Token, error))
	return execFunc(tokenStr)
}
func (mj MockJWT) GetToken(data map[string]interface{}) (*string, error) {
	f := mj.GetMockFunc("GetToken")
	execFunc := f.(func(data map[string]interface{}) (*string, error))
	return execFunc(data)
}
func (mj MockJWT) IsKid(kid string) bool {
	f := mj.GetMockFunc("IsKid")
	execFunc := f.(func(kid string) bool)
	return execFunc(kid)
}
