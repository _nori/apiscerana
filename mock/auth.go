package mock

import (
	"apiscerana/api/input"
	"apiscerana/dao/doc"
	"apiscerana/util"
)

const (
	CtxMockAppModel  = util.CtxKey("ctxMockAppModel")
	CtxMockAuthModel = util.CtxKey("ctxMockAuthModel")
)

type MockAppModel struct {
	BasicMock
}

func (am *MockAppModel) IsAppReview(platform, version string) bool {
	f := am.GetMockFunc("IsAppReview")
	execFunc := f.(func(platform, version string) bool)
	return execFunc(platform, version)
}

func (am *MockAppModel) CheckAppVers(app, version string) *doc.Version {
	f := am.GetMockFunc("CheckAppVers")
	execFunc := f.(func(app, version string) *doc.Version)
	return execFunc(app, version)
}

func (am *MockAppModel) CheckAppLangVers(lang, version string) *doc.Version {
	f := am.GetMockFunc("CheckAppLangVers")
	execFunc := f.(func(lang, version string) *doc.Version)
	return execFunc(lang, version)
}

func (am *MockAppModel) CheckDeskLangVers(lang, version string) *doc.Version {
	f := am.GetMockFunc("CheckDeskLangVers")
	execFunc := f.(func(lang, version string) *doc.Version)
	return execFunc(lang, version)
}

type MockAuthModel struct {
	BasicMock
}

func (am MockAuthModel) Login(i input.LoginV2) (*doc.User, error) {
	f := am.GetMockFunc("Login")
	execFunc := f.(func(i input.LoginV2) (*doc.User, error))
	return execFunc(i)
}

func (am MockAuthModel) Logout(u input.ReqUser, token string) error {
	f := am.GetMockFunc("Logout")
	execFunc := f.(func(u input.ReqUser, token string) error)
	return execFunc(u, token)
}
