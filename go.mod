module apiscerana

go 1.13

require (
	github.com/RichardKnop/machinery v1.10.3
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fluent/fluent-logger-golang v1.5.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.5.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/steinfletcher/apitest v1.5.0
	github.com/steinfletcher/apitest-jsonpath v1.6.0
	github.com/stretchr/testify v1.7.0
	github.com/tinylib/msgp v1.1.5 // indirect
	go.mongodb.org/mongo-driver v1.4.6
	gopkg.in/yaml.v2 v2.4.0
)
