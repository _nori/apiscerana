package v2

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"apiscerana/api/input"
	"apiscerana/core/api"
	"apiscerana/core/db"
	"apiscerana/dao/doc"
	"apiscerana/model"
	"apiscerana/util"

	"go.mongodb.org/mongo-driver/bson"
)

type AlertAPI string

func (a AlertAPI) GetName() string {
	return string(a)
}

func (a AlertAPI) Init() {

}

func (a AlertAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		{Path: "/v2/alert", Next: a.GetEndpoint, Method: "GET", Auth: true},
		{Path: "/v2/alert/{DEVICE}/received", Next: a.receivedEndpoint, Method: "POST", Auth: true},
	}
}

func (api *AlertAPI) receivedEndpoint(w http.ResponseWriter, req *http.Request) {
	// pathVars := util.GetPathVars(req, []string{"DEVICE"})
	// device := pathVars["DEVICE"].(string)
	// di := rsrc.GetDI()
	// mgodb := di.GetMongoByReq(req)
	// redis := di.GetRedisByReq(req)
	// gw := &doc.Gateway{}
	// err := gw.FindByID(mgodb, device)
	// if err != nil || gw == nil {
	// 	w.WriteHeader(http.StatusNotFound)
	// 	return
	// }
	// pid := req.Header.Get("AuthPID")
	// ra := cache.ReceivedAlert{
	// 	DeviceID: device,
	// 	PhoneID:  pid,
	// }

	// q := util.GetQueryValue(req, []string{"d"}, true)
	// time := (*q)["d"].(string)

	// err = ra.Cache(redis, time)
	// if err != nil {
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	getLog().Err(err.Error())
	// 	return
	// }

	// if ra.IsExist(redis) {
	// 	fmt.Println("exist")
	// }

	// w.Write([]byte("ok"))
}

func (api *AlertAPI) GetEndpoint(w http.ResponseWriter, req *http.Request) {
	queryValues := util.GetQueryValue(req, []string{"page", "limit"}, true)
	validateRule := map[string][]string{
		"page":  []string{"Numeric"},
		"limit": []string{"Numeric"},
	}
	ok, errMsg := util.CheckParam(queryValues, validateRule)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(errMsg)
		return
	}

	page, err := strconv.Atoi((queryValues)["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi((queryValues)["limit"].(string))
	if err != nil {
		limit = 100
	}

	ui := input.GetUserInfo(req)
	account := ui.GetAccount()
	upm := model.NewUserPermModelByReq(req)
	ids := upm.GetUserEquipIDs(account)

	if len(ids) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	q := bson.M{
		"equipid":  bson.M{"$in": ids},
		"hasalert": true,
		"_delete":  bson.M{"$exists": false},
	}

	mgom := model.NewMgoModelByReq(req, db.CoreDB)
	ps := mgom.GetAggrPaginationSource(&doc.SensorAggrEquipProject{}, q)
	userPermMap := upm.GetUserEquipPermMap(account)
	res, err := util.NewPagination(ps, int64(limit), int64(page), func(i interface{}) map[string]interface{} {
		if s, ok := i.(*doc.SensorAggrEquipProject); ok {
			equipName, projectName, equipID, projectID, permission := "", "", "", "", ""
			if !s.EquipmentObj.ID.IsZero() {
				equipName = s.EquipmentObj.Name
				equipID = s.EquipmentObj.ID.Hex()
				permission = userPermMap[equipID]
			}
			if !s.ProjectObj.ID.IsZero() {
				projectName = s.ProjectObj.Name
				projectID = s.ProjectObj.ID.Hex()
			}

			return map[string]interface{}{
				"equipName":   equipName,
				"sensorName":  s.Display,
				"sensorValue": fmt.Sprintf("%.1f %s", s.Value, s.Unit),
				"projectName": projectName,
				"alertTime":   s.AlertTime,
				"projectID":   projectID,
				"equipmentID": equipID,
				"sensorID":    s.ID.Hex(),
				"type":        s.SType,
				"value":       s.Value,
				"permission":  permission,
			}
		}
		return nil
	})

	if res == nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(res); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

}
