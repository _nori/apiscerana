package v1

import (
	"encoding/json"
	"net/http"

	"apiscerana/core/api"
	"apiscerana/model"
	"apiscerana/util"
)

type AuthAPI string

func (a AuthAPI) GetName() string {
	return string(a)
}

func (a AuthAPI) Init() {

}

func (a AuthAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		// &api.APIHandler{Path: "/v1/login", Next: api.loginEndpoint, Method: "POST", Auth: false},
		// &rsrc.APIHandler{Path: "/v1/pwd", Next: api.ChangePwdEndpoint, Method: "POST", Auth: true},
		// &rsrc.APIHandler{Path: "/v1/logout", Next: api.LogoutEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/app", Next: a.appEndpoint, Method: "GET", Auth: false},
		// &rsrc.APIHandler{Path: "/v1/msg", Next: api.MsgEndpoint, Method: "POST", Auth: true},
		// &rsrc.APIHandler{Path: "/v1/pushToken/{PhoneID}", Next: api.createPushTokenEndpoint, Method: "POST", Auth: true},
		// &rsrc.APIHandler{Path: "/v1/pushToken/{PhoneID}", Next: api.delPushTokenEndpoint, Method: "DELETE", Auth: true},
	}
}

// func (api *AuthAPI) createPushTokenEndpoint(w http.ResponseWriter, req *http.Request) {
// 	di := rsrc.GetDI()
// 	mgodb := di.GetMongoByReq(req)
// 	authID := req.Header.Get("AuthID")
// 	user := &doc.User{}
// 	err := user.FindByID(mgodb, authID)
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}

// 	vars := util.GetPathVars(req, []string{"PhoneID"})
// 	phoneID := vars["PhoneID"].(string)

// 	postValue, err := util.GetPostValue(req, true, []string{"push", "platform"})
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}

// 	validateRule := map[string][]string{
// 		"push":     []string{"Required"},
// 		"platform": []string{"Alpha", "Required"},
// 	}

// 	ok, errAry := util.CheckParam(*postValue, validateRule)
// 	if !ok {
// 		getLog().Debug(fmt.Sprintln(errAry))
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}

// 	push := (*postValue)["push"].(string)
// 	platform := (*postValue)["platform"].(string)

// 	sysCode := req.Header.Get("AuthSystem")

// 	err = doc.DelUserPushToken(mgodb, phoneID)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		w.WriteHeader(http.StatusInternalServerError)
// 		return
// 	}

// 	// 更新使用者PushToken
// 	err = user.SavePushTokenV2(mgodb, push, sysCode, platform, phoneID)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		w.WriteHeader(http.StatusInternalServerError)
// 		return
// 	}
// 	w.Write([]byte("OK"))
// }

// func (api *AuthAPI) delPushTokenEndpoint(w http.ResponseWriter, req *http.Request) {
// 	di := rsrc.GetDI()
// 	mgodb := di.GetMongoByReq(req)
// 	vars := util.GetPathVars(req, []string{"PhoneID"})
// 	queryPID := vars["PhoneID"].(string)
// 	err := doc.DelUserPushToken(mgodb, queryPID)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		w.WriteHeader(http.StatusInternalServerError)
// 		return
// 	}
// 	w.Write([]byte("OK"))
// }

// func (api AuthAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
// 	// 判斷使用者是否有token (末實作)
// 	// 從資料庫取出使用者帳號資料
// 	// 從Redis取出private key
// 	// 解密碼出來做MD5比對使用者密碼
// 	// 成功 - 產生token 將使用者資訊加密存入redis方便取用
// 	// 寫入redis
// 	isLogin := util.IsLogin(req)
// 	if isLogin {
// 		w.WriteHeader(http.StatusAccepted)
// 		return
// 	}
// 	showInfo := false
// 	queryValues := util.GetQueryValue(req, []string{"info"}, false)
// 	if info, ok := (*queryValues)["info"]; ok && info == "true" {
// 		showInfo = true
// 	}

// 	sysCode, ok := getSysCode(req)
// 	if !ok {
// 		getLog().Debug("system not set")
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}

// 	postValue, err := util.GetPostValue(req, true, []string{"account", "pwd", "push", "platform"})
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}

// 	validateRule := map[string][]string{
// 		"account":  []string{"Alphanumeric", "Required"},
// 		"pwd":      []string{"Required"},
// 		"platform": []string{"Alpha"},
// 	}

// 	ok, errAry := util.CheckParam(*postValue, validateRule)

// 	if !ok {
// 		getLog().Debug(fmt.Sprintln(errAry))
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	account := (*postValue)["account"].(string)
// 	userPwd := (*postValue)["pwd"].(string)
// 	userPwd = util.MD5(userPwd)

// 	di := rsrc.GetDI()
// 	mgodb := di.GetMongoByReq(req)
// 	user := &doc.User{}
// 	err = user.FindByAccAndPwd(mgodb, account, userPwd, sysCode)
// 	if err != nil {
// 		w.WriteHeader(http.StatusNotFound)
// 		w.Write([]byte("account or password error."))
// 		getLog().Debug(fmt.Sprintf("Login with: %s - %s - %s",
// 			account, userPwd, sysCode))
// 		getLog().Debug(err.Error())
// 		return
// 	}

// 	group := user.GetGroup(sysCode)
// 	if group == "" || user.Enable == false {
// 		w.WriteHeader(http.StatusNotFound)
// 		w.Write([]byte("Permission denied"))
// 		return
// 	}

// 	pushToken := (*postValue)["push"].(string)
// 	if pushToken != "" {
// 		platform := (*postValue)["platform"].(string)
// 		if !util.IsStrInList(platform, doc.PlatformAndroid, doc.PlatformIOS, doc.PlatformWEB) {
// 			w.WriteHeader(http.StatusBadRequest)
// 			w.Write([]byte("paltform error"))
// 			return
// 		}
// 		// 更新使用者PushToken
// 		user.SavePushToken(mgodb, pushToken, sysCode, platform)
// 	}

// 	token, err := user.GetToken(sysCode)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		w.WriteHeader(http.StatusInternalServerError)
// 		w.Write([]byte("Token generate fail"))
// 		return
// 	}
// 	getLog().Debug(fmt.Sprintf("Login token: %s", token))

// 	// set header for middleware to set toke
// 	req.Header.Add("SET_TOKEN", token)

// 	if showInfo {
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(
// 			map[string]string{
// 				"token":   token,
// 				"group":   group,
// 				"company": user.Company,
// 				"name":    user.Name,
// 			})
// 	} else {
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(
// 			map[string]string{
// 				"token": token,
// 				"name":  user.Name,
// 			})
// 	}
// }

// func (api AuthAPI) ChangePwdEndpoint(w http.ResponseWriter, req *http.Request) {
// 	userID := req.Header.Get("AuthID")
// 	if userID == "" {
// 		w.WriteHeader(http.StatusNotFound)
// 		return
// 	}
// 	di := rsrc.GetDI()
// 	mgodb := di.GetMongoByReq(req)
// 	userDoc := &doc.User{}
// 	err := userDoc.FindByID(mgodb, userID)
// 	if err != nil {
// 		w.WriteHeader(http.StatusNotFound)
// 		return
// 	}

// 	postVal, err := util.GetPostValue(req, false, []string{"new-pwd", "pwd"})
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	if len(*postVal) < 2 {
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	pwd := (*postVal)["pwd"].(string)
// 	newPwd := (*postVal)["new-pwd"].(string)
// 	md5pwd := util.MD5(pwd)
// 	if userDoc.Pwd != md5pwd {
// 		w.WriteHeader(http.StatusForbidden)
// 		return
// 	}
// 	_, err = util.IsValidPwd(newPwd)
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}

// 	updateField := bson.M{"pwd": util.MD5(newPwd)}
// 	if userDoc.State == doc.UserStateResetPwd {
// 		updateField["state"] = doc.UserStateNormal
// 	}

// 	err = userDoc.UpdateField(mgodb, updateField)
// 	if err != nil {
// 		w.WriteHeader(http.StatusInternalServerError)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}
// 	w.Write([]byte("success"))
// }

// func (api AuthAPI) LogoutEndpoint(w http.ResponseWriter, req *http.Request) {
// 	token := req.Header.Get("Token")
// 	redisClient := getRedisByReq(req)
// 	err := redisClient.Del(token).Err()
// 	if err != nil {
// 		w.WriteHeader(http.StatusOK)
// 		getLog().Err(err.Error())
// 		return
// 	}
// 	w.WriteHeader(http.StatusOK)
// }

func (a AuthAPI) appEndpoint(w http.ResponseWriter, req *http.Request) {
	queryValues := util.GetQueryValue(req, []string{"app", "app-v", "lang", "lang-v"}, true)
	validateRule := map[string][]string{
		"app":    []string{"Alpha", "Required"},
		"app-v":  []string{"Required"},
		"lang":   []string{"Required"},
		"lang-v": []string{"Required"},
	}

	ok, errArr := util.CheckParam(queryValues, validateRule)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(errArr)
		return
	}

	app := queryValues["app"].(string)
	appV := queryValues["app-v"].(string)
	lang := queryValues["lang"].(string)
	langV := queryValues["lang-v"].(string)

	resultMap := make(map[string]interface{})
	authModel := model.NewAppModelByReq(req)

	appVers := authModel.CheckAppVers(app, appV)
	if appVers != nil {
		resultMap["appV"] = appVers.Version
		resultMap["appURL"] = appVers.Url
	}

	langVers := authModel.CheckAppLangVers(lang, langV)
	if langVers != nil {
		resultMap["langV"] = langVers.Version
		resultMap["langURL"] = langVers.Url
	}

	resultMap["isReview"] = authModel.IsAppReview(app, appV)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resultMap)
}

// func (api AuthAPI) MsgEndpoint(w http.ResponseWriter, req *http.Request) {
// 	postValue, err := util.GetPostValue(req, true, []string{"email", "title", "content"})
// 	if err != nil {
// 		w.WriteHeader(http.StatusBadRequest)
// 		w.Write([]byte(err.Error()))
// 		return
// 	}

// 	validateRule := map[string][]string{
// 		"email": []string{"Email"},
// 	}
// 	required := []string{"email", "title", "content"}
// 	ok, errAry := util.CheckRequiredAndParam(*postValue, required, validateRule)

// 	if !ok {
// 		getLog().Debug(fmt.Sprintln(errAry))
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	title := html.EscapeString((*postValue)["title"].(string))
// 	content := html.EscapeString((*postValue)["content"].(string))
// 	di := rsrc.GetDI()
// 	message := doc.Message{
// 		From:    (*postValue)["email"].(string),
// 		To:      di.GetMailTo(),
// 		Title:   title,
// 		Content: content,
// 	}
// 	mongodb := di.GetMongoByReq(req)
// 	if err = message.Save(mongodb); err != nil {
// 		w.WriteHeader(http.StatusInternalServerError)
// 		getLog().Err(err.Error())
// 	}
// 	w.Write([]byte("success"))
// }
