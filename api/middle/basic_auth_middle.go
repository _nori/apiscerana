package middle

import (
	"apiscerana/api/input"
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/core/db"
	"apiscerana/util"
	"fmt"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	"github.com/gorilla/mux"
)

type authMiddle struct {
	name     string
	allowSys string
}

func NewAuthMiddle(name string) api.AuthMidInter {
	return &authMiddle{
		name: name,
	}
}

const (
	penddingMinute = 24 * 60 //閒置自動登出時間，單位分鐘

	authValue       = uint8(1 << iota)
	allowMultiValue = uint8(1 << iota)
	developValue    = uint8(1 << iota)

	AuthTokenKey = "Auth-Token"
)

var (
	authMap  map[string]uint8    = make(map[string]uint8)
	groupMap map[string][]string = make(map[string][]string)
)

func isDevelop(path string, method string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := authMap[key]

	if ok {
		return (value & developValue) > 0
	}
	return false
}

func isAuth(path string, method string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := authMap[key]

	if ok {
		return (value & authValue) > 0
	}
	return false
}

func hasPerm(path string, method string, perm string) bool {
	key := fmt.Sprintf("%s:%s", path, method)
	value, ok := groupMap[key]
	if len(value) == 0 {
		return true
	}
	if ok && util.IsStrInList(perm, value...) {
		return true
	}
	return false
}

func (am authMiddle) GetName() string {
	return am.name
}

func isDevelopToken(t *jwt.Token) bool {
	kid, ok := t.Header["kid"]
	if ok {
		return kid == "dev"
	}
	return false
}

func isUserToken(t *jwt.Token) bool {
	kid, ok := t.Header["kid"]
	di := core.GetDI()
	if ok {
		return di.GetJwt().IsKid(kid.(string))
	}
	return false
}

func (bam authMiddle) AddAuthPath(path string, method string, auth bool, group []string) {
	path = getPathKey(path, method)
	value := uint8(0)
	if auth {
		value = value | authValue
	}
	authMap[path] = uint8(value)
	groupMap[path] = group
}

func (bam *authMiddle) SetAllowSys(sys string) {
	bam.allowSys = sys
}

func getPathKey(path, method string) string {
	return fmt.Sprintf("%s:%s", path, method)
}

func (am *authMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			// ... pre handler functionality
			headerSys := r.Header.Get("System")
			if headerSys != "" {
				if util.MD5(am.allowSys) == headerSys {
					r.Header.Add(api.HeaderAuthSys, am.allowSys)
				} else {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("invalid System"))
					return
				}
			}
			path, err := mux.CurrentRoute(r).GetPathTemplate()
			if err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(err.Error()))
				return
			}
			di := core.GetDI()
			auth := isAuth(path, r.Method)
			j := di.GetJwt()
			if auth {
				authToken := r.Header.Get(AuthTokenKey)
				if authToken == "" {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("miss token"))
					return
				}
				jwtToken, err := j.Parse(authToken)
				if err != nil {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte(err.Error()))
					return
				}

				if !isUserToken(jwtToken) {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				mapClaims := jwtToken.Claims.(jwt.MapClaims)
				permission, ok := mapClaims["per"].(string)
				if hasPerm := hasPerm(path, r.Method, permission); ok && !hasPerm {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("permission error"))
					return
				}
				r.Header.Set("isLogin", "true")
				ru := input.NewReqUser(
					mapClaims["sub"].(string),
					mapClaims["nam"].(string),
					mapClaims["pid"].(string),
					permission,
				)
				r = util.SetCtxKeyVal(r, input.CtxUserInfoKey, ru)
			}
			develop := isDevelop(path, r.Method)
			if !auth && develop {
				authToken := r.Header.Get(AuthTokenKey)
				if authToken == "" {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("miss token"))
					return
				}

				jwtToken, err := j.Parse(authToken)
				if err != nil {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte(err.Error()))
					return
				}

				if !isDevelopToken(jwtToken) {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}

				mapClaims := jwtToken.Claims.(jwt.MapClaims)
				if sys, ok := mapClaims["sys"].(string); ok {
					if am.allowSys != sys {
						w.WriteHeader(http.StatusUnauthorized)
						return
					}
				}
			}
			f(w, r)
			setToken := r.Header.Get("SET_TOKEN")

			if setToken == "" {
				return
			}
			rc := db.GetCtxRedis(r)
			if rc == nil {
				return
			}
			device := util.GetClientKey(r)

			rc.Set(util.MD5(setToken), device, penddingMinute*time.Minute)
		}
	}
}
