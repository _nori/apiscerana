package doc

import (
	"apiscerana/core/dao"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

const (
	userPermC    = "userPermission"
	ConfirmPermC = "confirmPerm"

	PermissionBuyer       = "buyer"
	PermissionClassA      = "classA"
	PermissionClassB      = "classB"
	PermissionProject     = "project"
	PermissionProjectPlus = "project+"
)

type UserPermission struct {
	ID         primitive.ObjectID `bson:"_id"`
	Account    string
	ProjectID  primitive.ObjectID
	EquipID    *primitive.ObjectID
	GatewayID  primitive.ObjectID
	Permission string

	dao.CommonDoc `bson:"meta"`
}

func (u *UserPermission) GetDoc() interface{} {
	return u
}

func (u *UserPermission) GetC() string {
	return userPermC
}

func (u *UserPermission) GetID() primitive.ObjectID {
	return u.ID
}

func (u *UserPermission) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "account", Value: bsonx.String("text")}},
		},
	}
}
