package doc

import (
	"apiscerana/core/dao"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	appC = "app"

	PlatformIOS     = "ios"
	PlatformAndroid = "android"
	PlatformWEB     = "web"
	PlatformDesk    = "desk"
)

type App struct {
	ID       primitive.ObjectID `bson:"_id"`
	IOS      Version            `json:"ios" yaml:"ios,omitempty"`
	Android  Version            `json:"android" yaml:"android,omitempty"`
	Lang     map[string]Version `json:"lang" yaml:"lang,omitempty"`
	Desk     Version            `json:"desk" yaml:"desk,omitempty"`
	DeskLang map[string]Version `json:"deskLang" yaml:"deskLang,omitempty"`

	IOSReviw string `json:"iosReview" yaml:"iosReview"`

	dao.CommonDoc `bson:"meta"`
}

func (u *App) GetDoc() interface{} {
	return u
}

func (u *App) GetC() string {
	return appC
}

func (u *App) GetID() primitive.ObjectID {
	return u.ID
}

func (u *App) GetIndexes() []mongo.IndexModel {
	return nil
}

type Version struct {
	Version string `yaml:"version,omitempty"`
	Url     string `yaml:"url,omitempty"`
}

func (v *Version) IsValid(version string) bool {
	minVersion := versionToInt(v.Version)
	nowVersion := versionToInt(version)
	return nowVersion >= minVersion
}

// func RefreshAppInfo(mgo *mgo.Database) error {
// 	app := &App{}
// 	err := app.GetLatest(mgo)
// 	if err != nil {
// 		return err
// 	}
// 	appInfo = app
// 	return nil
// }

func versionToInt(version string) int {
	s := strings.Split(version, ".")
	length := len(s)
	if length != 3 {
		return 0
	}
	v1Str, v2Str, v3Str := s[0], s[1], s[2]

	v1, err := strconv.Atoi(v1Str)
	if err != nil {
		return 0
	}

	v2, err := strconv.Atoi(v2Str)
	if err != nil {
		return 0
	}

	v3, err := strconv.Atoi(v3Str)
	if err != nil {
		return 0
	}

	return (v1 << 16) + (v2 << 8) + v3
}
