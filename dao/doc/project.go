package doc

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	projectC = "project"

	ChargeBMode = "box"
	ChargePMode = "project"
)

type Project struct {
	ID          primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	Name        string             `json:"name"`    //案場名稱
	Address     string             `json:"address"` // 案場地址
	Coordinate  Coordinate         `json:"-"`
	Contact     Contact            `json:"contact"` // 案場聯絡人
	CountryCode string             `json:"countryCode"`
	CityCode    string             `json:"cityCode"`
	TownCode    string             `json:"townCode"`
	Boxes       []box              `json:"boxes"`
	DelBoxes    []box              `json:"delBoxes" bson:"-"`
	OwnerID     primitive.ObjectID `json:"ownerID,omitempty"`
	Charge      charge             `json:"charge"`
	SyncTag     []string           `json:"syncTag"`
	Equipments  []Equipment        `json:"-" bson:"lookupEquipment,omitempty"`
	OwnerObj    *User              `json:"-" bson:"-"`

	NecSync struct {
		Code string
	} `bson:"nec-sync"`
}

type charge struct {
	Mode string `json:"mode"`
}

type Contact struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

type Coordinate struct {
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type box struct {
	Name       string `json:"name"`
	MacAddress string `json:"macAddress"`
	BoxID      string `json:"boxID"`
	Timezone   string `json:"timezone"`
}
