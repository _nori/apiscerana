package doc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_UserTokenNewID(t *testing.T) {
	pk := UserPushToken{
		PushToken: "testToken",
	}
	id := pk.newID()
	assert.False(t, id.IsZero())
	assert.Equal(t, "72b507b15c2e61beb52c98e7", id.Hex())
}
