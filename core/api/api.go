package api

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type ApiError interface {
	GetStatus() int
	Error() string
}

type myApiError struct {
	StatusCode int
	Message    string
}

func (e myApiError) GetStatus() int {
	return e.StatusCode
}

func (e myApiError) Error() string {
	return fmt.Sprintf("%v: %v", e.StatusCode, e.Message)
}

func NewApiError(status int, msg string) ApiError {
	return myApiError{StatusCode: status, Message: msg}
}

type APIHandler struct {
	Path   string
	Next   func(http.ResponseWriter, *http.Request)
	Method string
	Auth   bool
	Group  []string
}

type API interface {
	GetAPIs() []*APIHandler
	GetName() string
	Init()
}

type APIConf struct {
	Port        string          `yaml:"port,omitempty"`
	EnableCORS  bool            `yaml:"cors"`
	Middle      map[string]bool `yaml:"middle,omitempty"`
	Apis        map[string]bool `yaml:"api,omitempty"`
	AllowSystem string          `yaml:"allowSys,omitempty"`
}

func NewTestApiConf() *APIConf {
	return &APIConf{
		AllowSystem: "test-sys",
	}
}

func (ac *APIConf) apiEnable(name string) bool {
	if v, ok := ac.Apis[name]; ok {
		return v
	}
	return true
}

func (ac *APIConf) middleEnable(name string) bool {
	if v, ok := ac.Middle[name]; ok {
		return v
	}
	return true
}

func (ac *APIConf) getMiddleList(ml []Middle) []Middleware {
	var middlewares []Middleware
	for _, m := range ml {
		if !ac.middleEnable(m.GetName()) {
			continue
		}
		middlewares = append(middlewares, m.GetMiddleWare())
	}
	return middlewares
}

func (ac *APIConf) GetRouter(
	authMiddle AuthMidInter,
	middles []Middle,
	apis ...API,
) *mux.Router {
	r := mux.NewRouter()

	ml := ac.getMiddleList(middles)
	if authMiddle != nil {
		authMiddle.SetAllowSys(ac.AllowSystem)
	}
	for _, myapi := range apis {
		if !ac.apiEnable(myapi.GetName()) {
			continue
		}
		myapi.Init()
		for _, handler := range myapi.GetAPIs() {
			if authMiddle != nil {
				authMiddle.AddAuthPath(handler.Path, handler.Method, handler.Auth, handler.Group)
			}
			r.HandleFunc(handler.Path, BuildChain(handler.Next, ml...)).Methods(handler.Method)
		}
	}
	return r
}

func (ac *APIConf) RunAPI(
	authMiddle AuthMidInter,
	middles []Middle,
	apis ...API,
) {
	r := ac.GetRouter(authMiddle, middles, apis...)
	port := "9080"
	if ac.Port != "" {
		port = ac.Port

	}
	log.Printf("start server at port %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), r))
}
