package que

import (
	"apiscerana/core/db"
	"apiscerana/core/log"

	"fmt"
	"sync"
	"time"

	machinery "github.com/RichardKnop/machinery/v1"
	amqpConf "github.com/RichardKnop/machinery/v1/config"
	"github.com/RichardKnop/machinery/v1/tasks"
	"github.com/google/uuid"
)

type Worker interface {
	Register(tasks map[string]interface{})
	Enable() bool
}

type QueServer interface {
	RegisterTasks(workers ...Worker) error
	StartWorker() error
	SendTask(taskName string, args []TaskArg) (TaskState, error)
}

type TaskArg tasks.Arg

type TaskState *tasks.TaskState

type QueConf amqpConf.Config

func (qc *QueConf) NewQueServer() (QueServer, error) {
	c := amqpConf.Config(*qc)
	taskServer, err := machinery.NewServer(&c)
	if err != nil {
		return nil, err
	}
	return &machQueServer{
		mserver: taskServer,
	}, nil
}

type machQueServer struct {
	mserver *machinery.Server
}

func (qs *machQueServer) RegisterTasks(workers ...Worker) error {
	taskMap := make(map[string]interface{})
	for _, t := range workers {
		if t.Enable() {
			t.Register(taskMap)
		}
	}
	return qs.mserver.RegisterTasks(taskMap)
}

func (qs *machQueServer) StartWorker() error {
	worker := qs.mserver.NewWorker(fmt.Sprintf("worker-%d", time.Now().UnixNano()), 1)
	err := worker.Launch()
	if err != nil {
		worker.Quit()
		return err
	}
	return nil
}

func (qs *machQueServer) SendTask(taskName string, args []TaskArg) (TaskState, error) {
	var targs []tasks.Arg
	for _, a := range args {
		targs = append(targs, tasks.Arg(a))
	}
	signature := &tasks.Signature{
		Name: taskName,
		Args: targs,
	}
	asyncResult, err := qs.mserver.SendTask(signature)
	if err != nil {
		return nil, err
	}
	return TaskState(asyncResult.GetState()), nil
}

type mockQueServer struct {
}

type QueueData interface {
	ToByte() []byte
}

type Queue interface {
	Append(d QueueData) error
}

type queImpl struct {
	curr int

	sync.RWMutex
	dataSlice []QueueData
	queueLen  int
	jobName   string

	redis db.RedisClient
	log   log.Logger
	task  QueServer
}

func NewQueue(
	task QueServer, redis db.RedisClient, log log.Logger, name string, qlen int,
) Queue {
	return &queImpl{
		task:     task,
		redis:    redis,
		log:      log,
		jobName:  name,
		queueLen: qlen,
	}
}

func (rq *queImpl) Append(d QueueData) error {
	rq.Lock()
	defer rq.Unlock()
	rq.dataSlice[rq.curr] = d
	rq.curr++
	rq.log.Debug(fmt.Sprintln("append data: ", d))
	rq.log.Debug(fmt.Sprintf("curr %d, max %d", rq.curr, rq.queueLen))
	if rq.curr == rq.queueLen {
		key, err := rq.cache()
		if err != nil {
			return err
		}
		rq.curr = 0
		err = rq.createJob(key)
		if err != nil {
			return err
		}
		rq.log.Debug(fmt.Sprintln("create job: ", key))
		return nil
	}
	return nil
}

func (rq *queImpl) cache() (string, error) {
	uid := uuid.New()
	for i := 0; i < rq.curr; i++ {
		_, err := rq.redis.LPush(uid.String(), rq.dataSlice[i].ToByte())
		if err != nil {
			rq.log.Err(err.Error())
			return "", err
		}
	}
	return uid.String(), nil
}

func (rq *queImpl) createJob(cache string) error {
	if rq.task == nil {
		panic("not set task")
	}
	rq.log.Debug("full and send task")

	result, err := rq.task.SendTask(rq.jobName, []TaskArg{
		{
			Type:  "string",
			Value: cache,
		}})
	if err != nil {
		return err
	}
	rq.log.Debug(fmt.Sprintf("Add Queue. UUID is %s. State is %s", result.TaskUUID, result.State))
	return nil
}

type QueReader interface {
	Pop(key string) []byte
}

func NewQueReader(rd db.RedisClient) QueReader {
	return &queReaderImpl{
		redis: rd,
	}
}

type queReaderImpl struct {
	redis db.RedisClient
}

func (rq *queReaderImpl) Pop(key string) []byte {
	b, err := rq.redis.RPop(key)
	if err != nil {
		rq.redis.Del(key)
		return nil
	}
	return b
}
