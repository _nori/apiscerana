package log

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/fluent/fluent-logger-golang/fluent"

	"apiscerana/util"
)

const (
	LogTargetFluent = "fluent"
)

type fluentLog struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Timezone string `yaml:"timezone"`
	Service  string `yaml:"service"`
}

func (rl *fluentLog) getClient() *fluent.Fluent {
	if os.Getenv("LOG_ENABLE") != "true" {
		return nil
	}
	host := os.Getenv("LOG_HOST")
	port := os.Getenv("LOG_PORT")

	if host == "" {
		host = rl.Host
	}

	if port == "" {
		port = rl.Port
	}

	p, err := strconv.Atoi(port)
	if err != nil {
		panic(err)
	}

	logger, err := fluent.New(fluent.Config{
		FluentHost: host,
		FluentPort: p,
	})
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return logger
}

var fluentTag = ""

func (rl *fluentLog) getTag() string {
	if fluentTag == "" {
		fluentTag = rl.Service + "-log"
	}
	return fluentTag
}

func (rl *fluentLog) Write(p []byte) (n int, err error) {
	splitMsg := strings.SplitN(string(p), " ", 5)

	msg := map[string]interface{}{
		"service":  rl.Service,
		"severity": splitMsg[0],
		"timeUnix": getTimeUnix(splitMsg[1], splitMsg[2], rl.Timezone),
		"filePath": splitMsg[3],
		"message":  strings.TrimSuffix(splitMsg[4], "\n"),
	}

	logger := rl.getClient()
	if logger == nil {
		return 0, nil
	}
	defer logger.Close()
	err = logger.Post(rl.getTag(), msg)
	if err != nil {
		fmt.Println(err)
	}
	return 0, nil
}

func getTimeUnix(d, t, z string) int64 {
	tt, err := time.Parse("2006/01/02 15:04:05.000000 -0700", util.StrAppend(d, " ", t, " ", z))
	if err != nil {
		fmt.Println(err.Error())
		return 0
	}
	return tt.Unix()
}
