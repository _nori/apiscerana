package model

import (
	"apiscerana/core/dao"
	"apiscerana/core/log"
	"apiscerana/core/que"
	"apiscerana/dao/doc"
	"apiscerana/util"
	"bytes"
	"encoding/gob"
	"fmt"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
)

type dataModelImpl struct {
	mdbm      MgoDBModel
	queReader que.QueReader
	log       log.Logger
}

var (
	ngGateway = map[string]string{
		"4C:1A:95:00:0C:98": "HK0001",
	}
)

func (dm *dataModelImpl) popData(queNo string) doc.Data {
	b := dm.queReader.Pop(queNo)
	r := doc.Data{}
	buffer := bytes.NewBuffer(b)
	err := gob.NewDecoder(buffer).Decode(&r)
	if err != nil {
		dm.log.Err(err.Error())
	}
	return r
}

func (dm *dataModelImpl) getControlSensor(macStr, gwidStr string) []*doc.Controller {
	boxMD5Str := doc.GetBoxMD5(macStr, gwidStr)
	query := bson.M{
		"boxmd5": boxMD5Str,
	}
	s := &doc.Controller{}
	r, err := dm.mdbm.Find(s, query)
	if err != nil {
		dm.log.Info(err.Error())
	}
	return r.([]*doc.Controller)
}

func (dm *dataModelImpl) Process(queNo string) error {
	dataParser := NewRawDataPaser()

	for data := dm.popData(queNo); data != nil; data = dm.popData(queNo) {

		if data.GetID().IsZero() {
			continue
		}
		gwid, ok := data["GW_ID"]
		if !ok {
			continue
		}
		mac, ok := data["MAC_Address"]
		if !ok {
			continue
		}
		timeStamp, ok := data["Timestamp_Unix"]
		if !ok {
			continue
		}
		gwidStr := gwid.(string)
		macStr := mac.(string)
		timeStampInt, err := getTimeStamp(timeStamp)
		if err != nil {
			dm.log.Warn(err.Error())
			continue
		}
		dm.fixNgGateway(macStr, gwidStr, data)
		dataParser.Load(data)
		cb := dao.BatchDoc{}
		if dataParser.HasSetMap() {
			cl := dm.getControlSensor(macStr, gwidStr)
			fmt.Println(cl, cb)
		}
		fmt.Println(timeStampInt)
	}
	return nil
}

func getTimeStamp(val interface{}) (int, error) {
	var timeStampInt int
	switch v := val.(type) {
	// 如果 value 是字串型態。
	case int:
		timeStampInt = val.(int)
	// 如果 value 是 int 型態。
	case int64:
		timeStampInt = int(val.(int64))
	case float64:
		timeStampInt = int(val.(float64))
	default:
		return 0, fmt.Errorf("error type: %s", v)
	}
	return timeStampInt, nil
}

func (dm *dataModelImpl) fixNgGateway(macAddress, gwid string, data doc.Data) {
	nggwid, ok := ngGateway[macAddress]
	if !ok || nggwid != gwid {
		return
	}
	exceptionField := []string{"GET_1_3", "GET_1_4", "GET_2_3", "GET_2_4", "GET_3_3", "GET_3_4", "Timestamp_Unix",
		"Storage_rate", "Timestamp", "MAC_Address", "CPU_rate", "GW_ID", "_id"}
	for k, v := range data {
		value, ok := v.(float64)
		if ok && !util.IsStrInList(k, exceptionField...) {
			data[k] = value / 10.0
		} else {
			dm.log.Debug(fmt.Sprintf("not float64: %s  %.f", k, value))
		}
	}
}

type SensorDataValue struct {
	Value     float64
	ValueType int
}

type rawDataPaser struct {
	data   doc.Data
	setMap map[uint16]SensorDataValue
	getMap map[uint16]SensorDataValue
}

func NewRawDataPaser() *rawDataPaser {
	r := &rawDataPaser{}
	return r
}

func (rdp *rawDataPaser) HasSetMap() bool {
	return len(rdp.setMap) > 0
}

func (rdp *rawDataPaser) Load(mydata doc.Data) {
	rdp.data = mydata
	rdp.getMap = make(map[uint16]SensorDataValue)
	rdp.setMap = make(map[uint16]SensorDataValue)
	var temp []string
	var device, address, valueType, tempLen int
	var dataValue float64
	var uniValue uint16
	var err error
	const GetTag, SetTag = "GET", "SET"
	for key, value := range mydata {
		temp = strings.Split(key, "_")
		tempLen = len(temp)
		if tempLen < 3 {
			continue
		}
		if !util.IsStrInList(temp[0], GetTag, SetTag) {
			continue
		}
		device, err = strconv.Atoi(temp[1])
		if err != nil || device < 0 || device > 256 {
			continue
		}
		address, err = strconv.Atoi(temp[2])
		if err != nil || address < 0 || address > 256 {
			continue
		}
		if tempLen == 4 {
			valueType, err = strconv.Atoi(temp[3])
			if err != nil {
				valueType = 0
			}
		} else {
			valueType = 0
		}
		dataValue = util.GetFloat64WithDP(value, valueType)

		uniValue = getSensorUniValue(uint8(device), uint8(address))
		switch temp[0] {
		case GetTag:
			rdp.getMap[uniValue] = SensorDataValue{
				Value:     dataValue,
				ValueType: valueType,
			}
		case SetTag:
			rdp.setMap[uniValue] = SensorDataValue{
				Value:     dataValue,
				ValueType: valueType,
			}
		}
	}
}

// 針對部份控制器，控制值會亂跑的調整
// type conMonitor struct {
// 	redis db.RedisClient
// 	db    db.MongoDBClient
// 	log   log.Logger
// }

// type sensorInter interface {
// 	SetValue(db *mgo.Database, value float64, u *doc.User) (bool, error)
// 	GetValue() float64
// 	GetDevice() uint8
// 	GetAddress() uint8
// }

// const (
// 	monitorKeyTpl = "monitor:%s:%d"
// 	field_Lower   = "lower"
// 	field_Upper   = "upper"
// )

// func NewExeConMonitor(r *redis.Client, db *mgo.Database, log *rsrc.Logger) *ConMonitor {
// 	return &conMonitor{
// 		redis: r,
// 		db:    db,
// 		log:   log,
// 	}
// }

// func (cm *conMonitor) RmCondition(mac, id string, s sensorInter) error {
// 	box := doc.GetBoxMD5(mac, id)
// 	uniValue := getSensorUniValue(s.GetDevice(), s.GetAddress())
// 	key := fmt.Sprintf(monitorKeyTpl, box, uniValue)
// 	return cm.redis.Del(key).Err()
// }

// func (cm *conMonitor) AddCondition(mac, id string, s sensorInter, lower, upper float64) error {
// 	box := doc.GetBoxMD5(mac, id)
// 	uniValue := getSensorUniValue(s.GetDevice(), s.GetAddress())
// 	key := fmt.Sprintf(monitorKeyTpl, box, uniValue)
// 	return cm.redis.HMSet(key, map[string]interface{}{
// 		field_Lower: lower,
// 		field_Upper: upper,
// 	}).Err()
// }

// func (cm *conMonitor) HasError(mac, id string, newValue float64, s sensorInter) bool {
// 	box := doc.GetBoxMD5(mac, id)
// 	uniValue := getSensorUniValue(s.GetDevice(), s.GetAddress())
// 	key := fmt.Sprintf(monitorKeyTpl, box, uniValue)

// 	if cm.redis.Exists(key).Val() == 0 {
// 		return false
// 	}
// 	valRange := cm.redis.HMGet(key, field_Lower, field_Upper).Val()
// 	if valRange[0] == nil || valRange[1] == nil {
// 		return false
// 	}

// 	lowerStr := valRange[0].(string)
// 	upperStr := valRange[1].(string)

// 	lowerf, err := strconv.ParseFloat(lowerStr, 64)
// 	if err != nil {
// 		return false
// 	}
// 	upperf, err := strconv.ParseFloat(upperStr, 64)
// 	if err != nil {
// 		return false
// 	}

// 	if lowerf <= newValue && upperf >= newValue {
// 		return false
// 	}

// 	cm.log.Warn(fmt.Sprintf("set error mac %s & id %s: new value is %f, set to %f", mac, id, newValue, s.GetValue()))
// 	_, err = s.SetValue(cm.db, s.GetValue(), nil)
// 	if err != nil {
// 		cm.log.Err(err.Error())
// 	}
// 	return true
// }

func getSensorUniValue(device, address uint8) uint16 {
	return uint16(device)*256 + uint16(address)
}
