package model

import (
	"fmt"
	"math"
	"time"

	"apiscerana/api/input"
	"apiscerana/core/log"
	"apiscerana/dao/doc"

	"go.mongodb.org/mongo-driver/bson"
)

type gatewayImpl struct {
	mgo MgoDBModel
	log log.Logger
}

func (gi gatewayImpl) SetVal(s *doc.Controller, value float64, u input.ReqUser) (bool, error) {
	gi.log.Info(fmt.Sprintf("sensor id: %s. Value is %f", s.ID.Hex(), value))
	if value < s.Lower || value > s.Upper {
		return false, fmt.Errorf("value is out of range: %.1f ~ %.1f", s.Lower, s.Upper)
	}
	nowTime := time.Now().Unix()
	limitSeconds := int64(2)
	if (nowTime - s.SetValueTime) < limitSeconds {
		gi.log.Debug(
			fmt.Sprintf("nowTime: %d, setValueTime: %d, duration: %d",
				nowTime, s.SetValueTime, limitSeconds))
		return false, fmt.Errorf("NotSetAble")
	}
	eq := &doc.Equipment{ID: s.EquipID}
	err := gi.mgo.FindByID(eq)
	if err != nil {
		gi.log.Err(fmt.Sprintln("cant not find equipment: ", s.EquipID.Hex()))
		return false, err
	}
	gw := &doc.Gateway{ID: s.GatewayID()}
	err = gi.mgo.FindByID(gw)
	if err != nil {
		gi.log.Err(fmt.Sprintf("Error: %s, MAC: %s, GWID: %s", err.Error(), eq.Box.MACaddress, eq.Box.BoxID))
		return false, err
	}
	setValue := value * math.Pow10(s.ValueType)

	// 更新時間

	uc, err := gi.mgo.UpdateOne(s, bson.D{{Key: "setvaluetime", Value: nowTime}}, u)
	fmt.Println(uc)
	s.SetValueTime = nowTime
	if err != nil {
		return false, err
	}

	// 更改數值
	key := fmt.Sprintf("SET_%d_%d_%d", s.Device, s.Address, s.ValueType)
	gi.mgo.UpdateOne(gw, bson.D{
		{Key: "conf", Value: map[string]interface{}{key: setValue}},
		{Key: "modifytimestamp", Value: time.Now().Unix()},
	}, u)

	if u != nil {
	}
	// 	// 新增日誌
	// 	ssv := sensorSetValue{
	// 		*s,
	// 		value,
	// 		u,
	// 	}
	// 	logOp, err := GetLogEquipmentTxn(mdb, ssv)
	// 	if err != nil {
	// 		return true, err
	// 	}
	// 	ops = append(ops, *logOp)
	// }

	// // 判斷是否為虛擬gateway
	// const virtualCmdTopic = "virtual/cmd"
	// if gw.IsVirtual() {
	// 	setMap := map[string]interface{}{
	// 		"GW_ID": gw.GwID,
	// 		key:     setValue,
	// 	}
	// 	err = rsrc.GetDI().GetMQTTGw().Publish([]string{virtualCmdTopic}, setMap, false)
	// 	getLog().Debug(fmt.Sprintln(virtualCmdTopic, setMap))
	// 	if err != nil {
	// 		getLog().Err(err.Error())
	// 	}
	// } else {
	// 	// mqtt推數值
	// 	topic := gw.GetMQTTRemotTopic()
	// 	err = rsrc.GetDI().GetMQTTGw().Publish([]string{topic}, map[string]interface{}{key: setValue}, false)
	// 	getLog().Debug(fmt.Sprintln(topic, key, setValue))
	// 	if err != nil {
	// 		getLog().Err(err.Error())
	// 	}
	// }

	// runner := txn.NewRunner(mdb.C(db.TxnC))
	// runner.SetOptions(txn.RunnerOptions{
	// 	MaxTxnQueueLength: 2000,
	// })
	// id := bson.NewObjectId() // Optional
	// err = runner.Run(ops, id, nil)
	// if err != nil {
	// 	return true, err
	// }
	return true, nil
}

type logSetVal struct {
	*doc.Controller
	newVal float64
	u      input.ReqUser
}
